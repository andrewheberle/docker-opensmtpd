FROM alpine:3.15

RUN apk add --no-cache opensmtpd

ENTRYPOINT [ "smtpd" ]

CMD [ "-d" ]
